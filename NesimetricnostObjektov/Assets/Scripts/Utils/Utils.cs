using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Fidersek
{
    public class Utils
    {
        public static Vector3[] F8Code = new Vector3[] {
            new Vector3(1,0,0),
            new Vector3(1,0,1),
            new Vector3(0,0,1),
            new Vector3(-1,0,1),
            new Vector3(-1,0,0),
            new Vector3(-1,0,-1),
            new Vector3(0,0,-1),
            new Vector3(1,0,-1),
        };

        public static double VolumeSign3D(Tris face, Vector3 point)
        {
            double[][] mat = { new double[]{ face.Point1.x,face.Point2.x,face.Point3.x,point.x },
                        new double[]{ face.Point1.y, face.Point2.y, face.Point3.y, point.y },
                        new double[]{ face.Point1.z, face.Point2.z, face.Point3.z,point.z },
                        new double[]{ 1, 1, 1, 1 } };
            double returnvalue = 1.0f / 6.0f * determinant(mat, 4);
            return returnvalue;
        }

        public static double VolumeSign2D(Vector3 point, Vector3 point2, Vector3 point3)
        {
            double[][] mat = {
                new double[]{ point.x,point2.x,point3.x },
                new double[]{ point.z,point2.z,point3.z },
                new double[]{ 1,1,1 }
            };
            double returnvalue = 1f / 6f * determinant(mat, 3);
            return returnvalue;
        }

        public static double determinant(double[][] m, int n)
        {
            double determinanta = 0;

            if (n == 1)
            {
                determinanta = m[0][0];
            }
            else if (n == 2)
            {
                determinanta = m[0][0] * m[1][1] - m[1][0] * m[0][1];
            }
            else
            {
                determinanta = 0;
                for (int j1 = 0; j1 < n; j1++)
                {
                    double[][] w = new double[n - 1][];
                    for (int k = 0; k < (n - 1); k++)
                    {
                        w[k] = new double[n - 1];
                    }
                    for (int i = 1; i < n; i++)
                    {
                        int j2 = 0;
                        for (int j = 0; j < n; j++)
                        {
                            if (j == j1)
                                continue;
                            w[i - 1][j2] = m[i][j];
                            j2++;
                        }
                    }
                    determinanta += Math.Pow(-1.0, 1.0 + j1 + 1.0)
                          * m[0][j1] * determinant(w, n - 1);
                }
            }
            return determinanta;
        }

        public static bool IsObjectSymmetrical(Line line, List<Vector3> hullPoints)
        {
            foreach (var item in hullPoints)
            {
                Vector3 mirroredPoint = MirrorPoint(line, item);
                if (!hullPoints.Exists(x => x == mirroredPoint))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsObjectSymmetrical(Plane plane, List<Vector3> hullPoints)
        {
            foreach (var item in hullPoints)
            {
                Vector3 mirroredPoint = (plane.ClosestPointOnPlane(item) - item) + plane.ClosestPointOnPlane(item);
                if (!hullPoints.Exists(x => x == mirroredPoint))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool PointOnLine(Line line, Vector3 currentPoint)
        {
            if (Vector3.Distance(line.Point1, line.Point2) == Vector3.Distance(line.Point1, currentPoint) + Vector3.Distance(line.Point2, currentPoint))
            {
                return true;
            }
            return false;
        }

        public static Vector3 MirrorPoint(Line line, Vector3 point)
        {
            //Deluje pravilno
            //Vector3 returnValue = line.Point1 + Vector3.Reflect(line.Point1 - point, (line.Point2 - line.Point1).normalized);
            return MirrorPointOverLine(line, point);
        }

        public static Vector3 MirrorPointOverLine(Line line, Vector3 point)
        {
            Vector3 premicaSmerni = line.Point2 - line.Point1;
            Vector3 premicaTocka = line.Point1;
            float a = premicaSmerni.x;
            float b = premicaSmerni.y;
            float c = premicaSmerni.z;
            float k = ((a * (point.x - premicaTocka.x) + b * (point.y - premicaTocka.y) + c * (point.z - premicaTocka.z)) / (a * a + b * b + c * c));

            float x = 2 * a * k + 2 * premicaTocka.x - point.x;
            float y = 2 * b * k + 2 * premicaTocka.y - point.y;
            float z = 2 * c * k + 2 * premicaTocka.z - point.z;

            return new Vector3(x, y, z);
        }

        public static void DrawLine(List<Vector3> hullPoints, LineRenderer parent)
        {
            parent.positionCount = hullPoints.Count + 1;
            parent.SetPositions(hullPoints.ToArray());
            parent.SetPosition(hullPoints.Count, hullPoints[0]);
        }

        public static bool IsInTriangle(Vector3 p, Tris tris)
        {
            Vector3 a = tris.Point1;
            Vector3 b = tris.Point2;
            Vector3 c = tris.Point3;

            Vector3 v0 = b - a, v1 = c - a, v2 = p - a;
            float d00 = Vector3.Dot(v0, v0);
            float d01 = Vector3.Dot(v0, v1);
            float d11 = Vector3.Dot(v1, v1);
            float d20 = Vector3.Dot(v2, v0);
            float d21 = Vector3.Dot(v2, v1);
            float denom = d00 * d11 - d01 * d01;
            float v = (d11 * d20 - d01 * d21) / denom;
            float w = (d00 * d21 - d01 * d20) / denom;
            float u = 1.0f - v - w;

            return v <= 1 && v >= 0 && w <= 1 && w >= 0 && u <= 1 && u >= 0 && v + w + u == 1;
        }

        public static void DestoryAllChildren(Transform t)
        {
            int childs = t.childCount;
            for (int i = childs - 1; i >= 0; i--)
            {
                GameObject.Destroy(t.GetChild(i).gameObject);
            }
        }

        public static bool IsCollinear(Vector3 point3, Vector3 point2, Vector3 point)
        {
            Vector3 side1 = point3 - point2;
            Vector3 side2 = point - point2;
            Vector3 cross = Vector3.Cross(side1, side2);
            return cross == Vector3.zero;
        }

        public static bool IsOnPlane(Tris face, Vector3 point)
        {
            Vector3 side1 = face.Point2 - face.Point1;
            Vector3 side2 = face.Point3 - face.Point1;
            Vector3 side3 = point - face.Point1;
            float d = Vector3.Dot(side1, Vector3.Cross(side2, side3));
            //Problems with floating values
            return Mathf.Abs(d) < 0.00000001f;
        }

        public static List<Vector3> LoadPlyFile(string path, out Mesh m)
        {
            List<Vector3> vertexList = new List<Vector3>();
            List<int> faceList = new List<int>();
            m = new Mesh();
            if (File.Exists(path))
            {

                StreamReader reader = new StreamReader(path);
                string inputLine = "";
                bool endHeader = false;
                int numberOfVertex = 0;
                int numberOfFaces = 0;

                while ((inputLine = reader.ReadLine()).Trim() != "end_header")
                {
                    inputLine = inputLine.Trim();
                    if (inputLine.Contains("element vertex"))
                    {
                        string[] splitString = inputLine.Split(new char[] { ' ' });
                        numberOfVertex = Int32.Parse(splitString[2]);
                    }
                    if (inputLine.Contains("element face"))
                    {
                        string[] splitString = inputLine.Split(new char[] { ' ' });
                        numberOfFaces = Int32.Parse(splitString[2]);
                    }
                }
                for (int i = 0; i < numberOfVertex; i++)
                {
                    (inputLine = reader.ReadLine()).Trim();
                    List<double> newRow = inputLine.Split(new char[] { ' ', '\t', '\n' }, StringSplitOptions.RemoveEmptyEntries).Select(x => double.Parse(x)).ToList();
                    vertexList.Add(new Vector3((float)newRow[0], (float)newRow[1], (float)newRow[2]));
                }
                for (int i = 0; i < numberOfFaces; i++)
                {
                    (inputLine = reader.ReadLine()).Trim();
                    List<int> newRow = inputLine.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries).Select(x => Int32.Parse(x)).ToList();
                    faceList.Add(newRow[1]);
                    faceList.Add(newRow[2]);
                    faceList.Add(newRow[3]);
                }
            }
            m.vertices = vertexList.ToArray();
            m.triangles = faceList.ToArray();
            return vertexList;
        }

        public static List<Vector3> Load2DPointsF8Format(string path)
        {
            List<Vector3> returnList = new List<Vector3>();

            if (File.Exists(path))
            {
                string content = File.ReadAllText(path, Encoding.UTF8);

                Vector3 startPoint = new Vector3();
                Vector3 avg = new Vector3();
                int pointCount = 0;
                //returnList.Add(startPoint);

                for (int i = 0; i < content.Length; i++)
                {
                    int value = content[i];
                    if (value > 55 || value < 48)
                    {
                        continue;
                    }
                    Vector3 newPoint = startPoint + (F8Code[value - 48] * (1f / 20f));
                    avg += newPoint;
                    pointCount++;
                    returnList.Add(newPoint);
                    startPoint = newPoint;
                }
                Vector3 center = avg / (float)pointCount;
                center.x = (float)(Math.Round((double)center.x, 2));
                center.y = (float)(Math.Round((double)center.y, 2));
                center.z = (float)(Math.Round((double)center.z, 2));
                for (int i = 0; i < returnList.Count; i++)
                {
                    returnList[i] -= center;
                }
            }
            return returnList;
        }
    }
}
