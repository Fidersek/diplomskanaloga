using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Fidersek
{
    public class SideBarController : MonoBehaviour
    {
        private Color green = Color.green;
        private Color yellow = Color.yellow;
        private Color white = Color.white;

        public List<FazaImage> faze;

        private void Start()
        {
            ConvexHullManager._instance.OnFazaChange += OnFazaChange;
        }

        private void Init()
        {
            foreach (var item in faze)
            {
                item.fazaImage.color = white;
            }
        }

        private void OnFazaChange(Faza f)
        {
            bool hasPassed = false;
            for (int i = 0; i < faze.Count; i++)
            {
                if (hasPassed)
                {
                    faze[i].fazaImage.color = white;
                }
                else if(f == faze[i].fazaType)
                {
                    faze[i].fazaImage.color = green;
                    hasPassed = true;
                }
                else
                {
                    faze[i].fazaImage.color = yellow;
                }

            }
        }

    }
    [System.Serializable]
    public class FazaImage
    {
        public Image fazaImage;
        public Faza fazaType;
    }
}
