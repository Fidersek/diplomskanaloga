using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fidersek
{
    public class UI_BottomBar : MonoBehaviour
    {
        public List<FazaObject> fazaObject;

        private void Start()
        {
            ConvexHullManager._instance.OnFazaChange += OnFazaChange;
        }

        private void OnFazaChange(Faza obj)
        {
            foreach (var item in fazaObject)
            {
                if (obj == item.faza)
                {
                    item.gameObject.SetActive(true);
                }
                else
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
    }
    [System.Serializable]
    public class FazaObject
    {
        public Faza faza;
        public GameObject gameObject;
    }
}
