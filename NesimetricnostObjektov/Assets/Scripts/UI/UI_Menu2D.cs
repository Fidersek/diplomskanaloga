using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fidersek
{
    public class UI_Menu2D : MonoBehaviour
    {
        public UI_Manager manager;

        public void OnReturnToMainMenuButtonClick()
        {
            manager.OpenMainMenu();
        }
    }
}
