using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fidersek
{
    public class UI_Manager : MonoBehaviour
    {
        public GameObject MainMenu;
        public GameObject Menu;

        public void Start()
        {
            OpenMainMenu();
        }

        public void OpenMainMenu()
        {
            MainMenu.SetActive(true);
            Menu.SetActive(false);
        }
        public void OpenMenu2D()
        {
            MainMenu.SetActive(false);
            Menu.SetActive(true);
            ConvexHullManager._instance.type = DimensionType._2D;
            ConvexHullManager._instance.FazaVnos();
        }
        public void OpenMenu3D()
        {
            MainMenu.SetActive(false);
            Menu.SetActive(true);
            ConvexHullManager._instance.type = DimensionType._3D;
            ConvexHullManager._instance.FazaVnos();
        }

    }
}
