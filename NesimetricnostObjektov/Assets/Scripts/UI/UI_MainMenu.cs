using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fidersek
{
    public class UI_MainMenu : MonoBehaviour
    {

        public UI_Manager manager;

        public void On2dButtonClick()
        {
            manager.OpenMenu2D();
        }
        public void On3dButtonClick()
        {
            manager.OpenMenu3D();
        }
        public void OnExitButtonClick()
        {
            #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
            #else
                Application.Quit();
            #endif
        }
    }
}
