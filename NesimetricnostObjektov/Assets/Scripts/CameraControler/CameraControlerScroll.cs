using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Fidersek
{
    public class CameraControlerScroll : MonoBehaviour
    {
        public float zoom=4;
        private float maxZoom=20;
        private float minZoom=4;


        private CinemachineFreeLook freelook;
        private CinemachineFreeLook.Orbit[] originalOrbits;

        public void Start()
        {
            freelook = GetComponentInChildren<CinemachineFreeLook>();
            originalOrbits = new CinemachineFreeLook.Orbit[freelook.m_Orbits.Length];
            for (int i = 0; i < freelook.m_Orbits.Length; i++)
            {
                originalOrbits[i].m_Height = freelook.m_Orbits[i].m_Height;
                originalOrbits[i].m_Radius = freelook.m_Orbits[i].m_Radius;
            }
        }

        public void UpdateOrbit(float distance)
        {
            freelook.m_Orbits[0].m_Height = distance;
            freelook.m_Orbits[0].m_Radius = 0;

            freelook.m_Orbits[1].m_Height = 0;
            freelook.m_Orbits[1].m_Radius = distance;

            freelook.m_Orbits[2].m_Height = -distance;
            freelook.m_Orbits[2].m_Radius = 0;
        }


        public void MouseScroll(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                float value = context.ReadValue<float>();
                if (value == 1)
                {
                    if (zoom > minZoom)
                    {
                        zoom--;
                    }

                    UpdateOrbit(zoom);
                }
                else if (value == -1)
                {
                    if (zoom < maxZoom)
                    {
                        zoom++;
                    }
                    UpdateOrbit(zoom);
                }
            }

        }


    }
}
