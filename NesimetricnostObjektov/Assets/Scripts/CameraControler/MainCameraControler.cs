using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fidersek
{
    public class MainCameraControler : MonoBehaviour
    {
        public GameObject Camera2DGameObject;
        public GameObject Camera3DGameObject;


        public void TurnOn2D()
        {
            Camera2DGameObject.SetActive(true);
            Camera3DGameObject.SetActive(false);
        }
        public void TurnOn3D()
        {
            Camera2DGameObject.SetActive(false);
            Camera3DGameObject.SetActive(true);
        }
    }
}
