using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Fidersek
{
    public class Camera2DScroll : MonoBehaviour
    {
        private CinemachineVirtualCamera virtualCamera2D;

        public float zoom = 4;
        private float maxZoom = 100;
        private float minZoom = 4;


        private void Start()
        {
            UpdateOrbit(zoom);
        }

        public void MouseScroll(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                float value = context.ReadValue<float>();
                if (value == 1)
                {
                    if (zoom > minZoom)
                    {
                        zoom--;
                    }

                    UpdateOrbit(zoom);
                }
                else if (value == -1)
                {
                    if (zoom < maxZoom)
                    {
                        zoom++;
                    }
                    UpdateOrbit(zoom);
                }
            }

        }

        private void UpdateOrbit(float zoom)
        {
            Vector3 currentPostion = transform.position;
            transform.position = new Vector3(currentPostion.x, zoom, currentPostion.z);
        }
    }
}
