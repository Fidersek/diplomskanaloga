using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fidersek
{
    public class PointDrawer : MonoBehaviour
    {
        public Mesh pointMesh;
        public Material redPoint;
        public Material greenPoint;
        public Material grayPoint;

        private Vector3 inputPointScale = new Vector3(0.049f, 0.049f, 0.049f);
        private Vector3 hullPointScale = new Vector3(0.05f, 0.05f, 0.05f);

        private List<List<Matrix4x4>> greenBatch = new List<List<Matrix4x4>>();
        private List<List<Matrix4x4>> grayBatch = new List<List<Matrix4x4>>();
        private List<List<Matrix4x4>> redBatch = new List<List<Matrix4x4>>();

        public bool ShowHull = true;
        public bool ShowInput = true;

        public void Update()
        {
            if (ShowInput)
            {
                RenderInputPoints();
            }
            if (ShowHull)
            {
                RenderHullPoints();
            }
        }

        public void ClearBatch()
        {
            greenBatch = new List<List<Matrix4x4>>();
            grayBatch = new List<List<Matrix4x4>>();
            redBatch = new List<List<Matrix4x4>>();
        }

        private void RenderInputPoints()
        {
            foreach (var batch in grayBatch)
            {
                Graphics.DrawMeshInstanced(pointMesh, 0, grayPoint, batch);
            }

        }

        private void RenderHullPoints()
        {
            foreach (var batch in greenBatch)
            {
                Graphics.DrawMeshInstanced(pointMesh, 0, greenPoint, batch);
            }
            foreach (var batch in redBatch)
            {
                Graphics.DrawMeshInstanced(pointMesh, 0, redPoint, batch);
            }
        }

        public void SetHullPoints(List<Vector3> greenPointsData, List<Vector3> redPointsData)
        {
            redBatch = new List<List<Matrix4x4>>();
            greenBatch = new List<List<Matrix4x4>>();

            BuildBatch(greenPointsData, greenBatch, hullPointScale);
            BuildBatch(redPointsData, redBatch, hullPointScale);
        }
        public void SetInputPoints(List<Vector3> inputPointsData)
        {
            grayBatch = new List<List<Matrix4x4>>();
            BuildBatch(inputPointsData, grayBatch, inputPointScale);
        }

        public void BuildBatch(List<Vector3> pointData, List<List<Matrix4x4>> batchHolder,Vector3 scale)
        {
            int addedPoints = 0;

            for (int i = 0; i < pointData.Count; i++)
            {
                if (addedPoints < 1000 && batchHolder.Count != 0)
                {
                    batchHolder[batchHolder.Count - 1].Add(Matrix4x4.TRS(pointData[i], Quaternion.identity, scale));
                    addedPoints += 1;
                }
                else
                {
                    batchHolder.Add(new List<Matrix4x4>());
                    batchHolder[batchHolder.Count - 1].Add(Matrix4x4.TRS(pointData[i], Quaternion.identity, scale));
                    addedPoints = 1;
                }
            }
        }


        public void ShowInputPoints(bool show)
        {
            ShowInput = show;
        }
        public void ShowHullPoints(bool show)
        {
            ShowHull = show;
        }
    }
    public class ObjData
    {
        public Vector3 pos;
        public Vector3 scale;
        public Quaternion rot;

        public Matrix4x4 matrix
        {
            get
            {
                return Matrix4x4.TRS(pos, rot, scale);
            }
        }
    }

}
