using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fidersek
{
    public class BigMeshRenderer : MonoBehaviour
    {
        public MeshFilter ImportObjectMesh;
        public MeshFilter HullMesh;

        public GameObject ImportMeshObject;
        public GameObject ConvexHullMeshObject;

        public void Start()
        {
            ImportObjectMesh.mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
            HullMesh.mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
            ShowHullMesh(true);
            ShowImportMesh(true);
        }

        public void SetImportMesh(Mesh newMesh)
        {
            ImportObjectMesh.mesh = newMesh;
        }

        public void SetHullMesh(Mesh newMesh)
        {
            HullMesh.mesh = newMesh;
        }


        public void ShowHullMesh(bool show)
        {
            ImportMeshObject.SetActive(show);
        }
        public void ShowImportMesh(bool show)
        {
            ImportMeshObject.SetActive(show);
        }
    }
}
