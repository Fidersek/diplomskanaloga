﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Fidersek.Assets.Scripts.Simetricnost.Base;
using System;
using Random = UnityEngine.Random;
using SimpleFileBrowser;
using System.IO;
using Dummiesman;

namespace Fidersek
{
    public class ConvexHullManager : MonoBehaviour
    {
        public static ConvexHullManager _instance;

        void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
            else
            {
                Destroy(this);
            }
        }

        [Header("2D podatki")]
        //2D data
        public List<Vector3> inputPoints2D;
        public ConvexHull2D convexHull2D;

        [Header("3D podatki")]
        //3D data
        public List<Vector3> inputPoints3D;
        public ConvexHull3D convexHull3D;

        [Header("Input")]
        //Input
        public TMP_InputField pointCount;
        public DimensionType type = DimensionType._2D;
        public Faza faza = Faza.VnosTock;

        [Header("Objekti")]
        //Output
        public MeshFilter FileLoaded;
        public LineRenderer IzbocenaLupina2D;
        public MeshFilter IzbocenaLupina3D;
        public GameObject OsiSimetrije;

        [Header("Objekti")]
        public TextMeshProUGUI resultText;
        public TextMeshProUGUI osiText;

        [Header("Prefabs")]
        public GameObject RedDot;
        public GameObject GreenDot;
        public GameObject GrayDot;
        public GameObject LineGreen;
        public GameObject Plane;

        public PointDrawer pointDrawer;
        public MainCameraControler mainControler;

        public Action<Faza> OnFazaChange;

        //Initialize
        public void Start()
        {
            OnFazaChange += FazaChange;
            Init();
        }

        public void Init()
        {
            inputPoints2D = new List<Vector3>();
            inputPoints3D = new List<Vector3>();
            convexHull2D = null;
            convexHull3D = null;
            pointCount.characterValidation = TMP_InputField.CharacterValidation.Integer;
            FileLoaded.mesh = null;
            pointDrawer.ClearBatch();
            Utils.DestoryAllChildren(OsiSimetrije.transform);
            IzbocenaLupina2D.SetPositions(new Vector3[] { });
            IzbocenaLupina2D.positionCount = 0;
            IzbocenaLupina3D.mesh = null;
            resultText.text = "";
            osiText.text = "";
        }

        //Generate Points
        public void GeneratePoints()
        {
            if (DimensionType._2D == type)
            {
                Generate2DPoints();
            }
            if (DimensionType._3D == type)
            {
                Generate3DPoints();
            }
        }
        public void Generate2DPoints()
        {
            inputPoints2D.Clear();
            int count = int.Parse(pointCount.text);
            for (int i = 0; i < count; i++)
            {
                inputPoints2D.Add(Generate2DPoint());
            }
            pointDrawer.SetInputPoints(inputPoints2D);
        }
        public void Generate3DPoints()
        {
            inputPoints3D.Clear();
            int count = int.Parse(pointCount.text);
            for (int i = 0; i < count; i++)
            {
                inputPoints3D.Add(Generate3DPoint());
            }
            pointDrawer.SetInputPoints(inputPoints3D);
        }
        private Vector3 Generate2DPoint()
        {
            return new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f));
        }
        private Vector3 Generate3DPoint()
        {
            return new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        }

        //Import Points get points from obj file
        public void Load2DPoints()
        {
            OpenLoadSelector();
        }

        public void Load3DPoints()
        {
            OpenLoadSelector();
        }

        public void MakeHull()
        {
            System.Diagnostics.Stopwatch st = new System.Diagnostics.Stopwatch();
            st.Start();
            if (DimensionType._2D == type)
            {
                Make2DHull();
            }
            if (DimensionType._3D == type)
            {
                Make3DHull();
            }
            st.Stop();
            Debug.Log(string.Format("Inkrementalni algoritem se je izvajal {0} ms", st.ElapsedMilliseconds));
        }

        public void Make2DHull()
        {
            if (convexHull2D != null && convexHull2D.generated)
            {
                return;
            }
            convexHull2D = new ConvexHull2D(inputPoints2D);
            //IzbocenaLupina2D.mesh = convexHull2D.GetMesh();
            pointDrawer.SetHullPoints(convexHull2D.HullPoints, convexHull2D.HullPointsInside);
            Utils.DrawLine(convexHull2D.HullPoints, IzbocenaLupina2D);
        }

        public void Make3DHull()
        {
            if (convexHull3D != null && convexHull3D.generated)
            {
                return;
            }
            convexHull3D = new ConvexHull3D(inputPoints3D, IzbocenaLupina3D);
            IzbocenaLupina3D.mesh = convexHull3D.GetMesh();
            pointDrawer.SetHullPoints(convexHull3D.HullPoints, convexHull3D.HullPointsInside);
        }

        public void FindSymmtryAxises()
        {
            System.Diagnostics.Stopwatch st = new System.Diagnostics.Stopwatch();
            st.Start();
            if (DimensionType._2D == type)
            {
                Find2DSymmtryAxises();
                osiText.text = "Točke imajo " + convexHull2D.SymmtryLines.Count + " osi simetrije.";
            }
            if (DimensionType._3D == type)
            {
                Find3DSymmtryAxises();
                osiText.text = "Objekt ima " + convexHull3D.SymmtryPlanes.Count + " ravnin simetrije.";
            }
            st.Stop();
            Debug.Log(string.Format("Iskanje simetričnih osi se je izvajal {0} ms", st.ElapsedMilliseconds));
        }

        public void Find2DSymmtryAxises()
        {
            convexHull2D.CheckForNonSymmtry();
            SpawnSymmtryLines(convexHull2D.SymmtryLines, LineGreen, OsiSimetrije);
        }

        public void Find3DSymmtryAxises()
        {
            convexHull3D.CheckForNonSymmtry();
            SpawnSymmtryPlanes(convexHull3D.SymmtryPlanes, convexHull3D.GetCenterPoint(), OsiSimetrije, Plane);
        }

        public void ChangeFaza(Faza f)
        {
            faza = f;
            OnFazaChange?.Invoke(f);
        }

        private void FazaChange(Faza obj)
        {
            if (obj == Faza.VnosTock)
            {
                Init();
            }
        }

        public void FazaLupina()
        {
            if ((type == DimensionType._2D && inputPoints2D != null && inputPoints2D.Count != 0) || (type == DimensionType._3D && inputPoints3D != null && inputPoints3D.Count != 0))
            {
                ChangeFaza(Faza.GeneriranjeLupine);
            }
            else
            {
                //Warnning message ni tock za lupino
            }
        }
        public void FazaOsi()
        {
            if ((type == DimensionType._2D && convexHull2D != null) || (type == DimensionType._3D && convexHull3D != null))
            {
                ChangeFaza(Faza.SimetricneOsi);
            }
            else
            {
                //Warnning message ni lupine
            }

        }
        public void FazaRezultat()
        {
            if ((type == DimensionType._2D && convexHull2D != null && convexHull2D.SymmtryLines != null) || (type == DimensionType._3D && convexHull3D != null && convexHull3D.SymmtryPlanes != null))
            {
                SetRezultatText();
                ChangeFaza(Faza.Rezultat);
            }
            else
            {
                //Warnning message ni osi simmetrije
            }

        }

        private void SetRezultatText()
        {
            if ((type == DimensionType._2D && convexHull2D.SymmtryLines.Count > 0) || (type == DimensionType._3D && convexHull3D.SymmtryPlanes.Count > 0))
            {
                resultText.text = "Dokazali smo, da je objekt lahko simetričen saj ima izbočeno lupino z " + (type == DimensionType._3D ? convexHull3D.SymmtryPlanes.Count : convexHull2D.SymmtryLines.Count).ToString() + " osi simetrije.";
            }
            else
            {
                resultText.text = "Dokazali smo, da je objekt nesimetričen saj ima izbočeno lupino z 0 osi simetrije.";
            }
        }

        public void FazaVnos()
        {
            ChangeFaza(Faza.VnosTock);
        }

        public void ShowLoadedObject(bool show)
        {
            FileLoaded.gameObject.SetActive(show);
        }
        public void ShowPoints(bool show)
        {
            pointDrawer.ShowInputPoints(show);
        }
        public void ShowLupina(bool show)
        {
            IzbocenaLupina3D.gameObject.SetActive(show);
            IzbocenaLupina2D.gameObject.SetActive(show);
        }
        public void ShowSimetrijskeOsi(bool show)
        {
            OsiSimetrije.SetActive(show);
        }
        public void ShowPointsLupina(bool show)
        {
            pointDrawer.ShowHullPoints(show);
        }

        private void SpawnSymmtryLines(List<Line> symmtryLines, GameObject lineobject, GameObject parent)
        {
            foreach (var item in symmtryLines)
            {
                Vector3 smerniVektor = (item.Point1 - item.Point2).normalized;
                Line premica = new Line(item.Point1 + smerniVektor * 1000, item.Point1 - smerniVektor * 1000);
                LineRenderer r = Instantiate(lineobject, Vector3.zero, Quaternion.identity, parent.transform).GetComponent<LineRenderer>();
                r.positionCount = 2;
                r.SetPosition(0, premica.Point1);
                r.SetPosition(1, premica.Point2);
            }
        }

        private void SpawnSymmtryPlanes(List<Plane> symmtryPlanes, Vector3 center, GameObject parent, GameObject planePrefab)
        {
            foreach (var item in symmtryPlanes)
            {
                Mesh planeMesh = GetPlaneMesh(item.normal, center);
                MeshFilter newPlaneMeshFilter = Instantiate(planePrefab, Vector3.zero, Quaternion.identity, parent.transform).GetComponent<MeshFilter>();
                newPlaneMeshFilter.mesh = planeMesh;
            }
        }

        private Mesh GetPlaneMesh(Vector3 normal, Vector3 position)
        {
            Mesh returnMesh = new Mesh();
            float planeSize = 3f;


            Vector3 helperVector;
            if (normal.normalized != Vector3.forward)
                helperVector = Vector3.Cross(normal, Vector3.forward).normalized * normal.magnitude;
            else
                helperVector = Vector3.Cross(normal, Vector3.up).normalized * normal.magnitude; ;

            var q = Quaternion.AngleAxis(45.0f, normal);
            helperVector = q * helperVector;
            helperVector *= planeSize;
            var corner0 = position + helperVector;
            var corner2 = position - helperVector;
            q = Quaternion.AngleAxis(90.0f, normal);
            helperVector = q * helperVector;
            var corner1 = position + helperVector;
            var corner3 = position - helperVector;

            List<Vector3> verts = new List<Vector3>() { corner0, corner1, corner2, corner3 };
            List<int> tri = new List<int>() { 0, 1, 2, 2, 3, 0, 2, 1, 0, 0, 3, 2 };

            returnMesh.vertices = verts.ToArray();
            returnMesh.triangles = tri.ToArray();

            return returnMesh;

        }

        [ContextMenu("OpenFile")]
        public void OpenLoadSelector()
        {


            if (type == DimensionType._2D)
            {
                FileBrowser.SetFilters(true, new FileBrowser.Filter("CodeChainF8", ".txt"));

                FileBrowser.SetDefaultFilter(".txt");

                FileBrowser.AddQuickLink("Users", "C:\\Users", null);

                StartCoroutine(Show2DLoadDialogCoroutine());
            }
            else if (type == DimensionType._3D)
            {
                FileBrowser.SetFilters(true, new FileBrowser.Filter("Objects", ".obj"), new FileBrowser.Filter("Stanford", ".ply"));

                FileBrowser.SetDefaultFilter(".ply");

                FileBrowser.AddQuickLink("Users", "C:\\Users", null);

                StartCoroutine(Show3DLoadDialogCoroutine());
            }

        }

        IEnumerator Show2DLoadDialogCoroutine()
        {

            yield return FileBrowser.WaitForLoadDialog(FileBrowser.PickMode.Files, true, null, null, "Load Files and Folders", "Load");

            GameObject loadedObject = new GameObject();

            if (FileBrowser.Success)
            {
                if (!File.Exists(FileBrowser.Result[0]))
                {
                    Debug.Log("File doesn't exists");
                }
                if (FileBrowser.Result[0].Contains(".txt"))
                {
                    inputPoints2D = Utils.Load2DPointsF8Format(FileBrowser.Result[0]);
                    pointDrawer.SetInputPoints(inputPoints2D);
                }
            }
            Destroy(loadedObject);
        }

        IEnumerator Show3DLoadDialogCoroutine()
        {

            yield return FileBrowser.WaitForLoadDialog(FileBrowser.PickMode.Files, true, null, null, "Load Files and Folders", "Load");

            GameObject loadedObject = new GameObject();

            if (FileBrowser.Success)
            {
                if (!File.Exists(FileBrowser.Result[0]))
                {
                    Debug.Log("File doesn't exists");
                }
                else if (FileBrowser.Result[0].Contains(".obj"))
                {
                    loadedObject = new OBJLoader().Load(FileBrowser.Result[0]);
                    Mesh mesh = loadedObject.transform.GetChild(0).GetComponent<MeshFilter>().mesh;
                    FileLoaded.mesh = mesh;

                    inputPoints3D = new List<Vector3>();

                    foreach (var item in mesh.vertices)
                    {
                        if (!inputPoints3D.Contains(item))
                        {
                            inputPoints3D.Add(item);
                        }
                    }
                    pointDrawer.SetInputPoints(inputPoints3D);
                }
                else if (FileBrowser.Result[0].Contains(".ply"))
                {
                    Mesh plyMesh;
                    inputPoints3D = Utils.LoadPlyFile(FileBrowser.Result[0],out plyMesh);
                    pointDrawer.SetInputPoints(inputPoints3D);
                    FileLoaded.mesh = plyMesh;
                }
            }
            Destroy(loadedObject);
        }

    }
    public enum Faza
    {
        VnosTock,
        GeneriranjeLupine,
        SimetricneOsi,
        Rezultat,
    }
    public enum DimensionType
    {
        _2D,
        _3D,
    }
}
