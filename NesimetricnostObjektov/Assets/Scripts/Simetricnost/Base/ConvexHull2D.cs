﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Fidersek.Assets.Scripts.Simetricnost.Base
{
    [System.Serializable]
    public class ConvexHull2D
    {
        public List<Vector3> HullPoints;
        public List<Vector3> HullPointsInside;
        public List<Line> HullLines;

        public List<Line> SymmtryLines;
        public List<Line> NonSymmtryLines;

        public bool generated = false;


        public ConvexHull2D(List<Vector3> points)
        {
            HullPoints = new List<Vector3>();
            HullLines = new List<Line>();
            HullPointsInside = new List<Vector3>();

            //Zacenti trikotnik
            if (!ConvexHull3DInitialTriangle(ref points))
            {
                return;
            }

            //Increment tocke
            while (points.Count != 0)
            {
                AddPointToHull(points);
            }

            generated = true;
        }

        public void CheckForNonSymmtry()
        {
            Vector3 center = GetCenterPoint();
            SymmtryLines = new List<Line>();
            NonSymmtryLines = new List<Line>();
            List<Line> Temp = new List<Line>();
            foreach (var item in HullPoints)
            {
                bool isCollinear = false;
                foreach (var line in Temp)
                {
                    if (Utils.IsCollinear(line.Point1, line.Point2, item))
                    {
                        isCollinear = true;
                        continue;
                    }
                }
                if (!isCollinear)
                {
                    Temp.Add(new Line(item, center));
                }
            }
            foreach (var item in HullLines)
            {
                Vector3 lineMiddle = item.GetMiddle();
                bool isCollinear = false;
                foreach (var line in Temp)
                {
                    if (Utils.IsCollinear(line.Point1, line.Point2, lineMiddle))
                    {
                        isCollinear = true;
                        continue;
                    }
                }
                if (!isCollinear)
                {
                    Temp.Add(new Line(lineMiddle, center));
                }
            }

            foreach (var item in Temp)
            {
                if (!Utils.IsObjectSymmetrical(item, HullPoints))
                {
                    NonSymmtryLines.Add(item);
                }
                else
                {
                    SymmtryLines.Add(item);
                }
            }
        }

        private void AddPointToHull(List<Vector3> points)
        {
            List<Vector3> neighborPoints = new List<Vector3>();
            List<Vector3> removedPoints = new List<Vector3>();
            List<Line> visibleLines = new List<Line>();

            Vector3 currentPoint = points[0];
            points.RemoveAt(0);

            for (int i = 0; i < HullLines.Count; i++)
            {
                double volumeSing = Utils.VolumeSign2D(HullLines[i].Point1, HullLines[i].Point2, currentPoint);

                if (volumeSing <= 0)
                {
                    if (volumeSing == 0 && Utils.PointOnLine(HullLines[i], currentPoint))
                    {
                        visibleLines = new List<Line>();
                        break;
                    }
                    
                    visibleLines.Add(HullLines[i]);

                    if (!neighborPoints.Contains(HullLines[i].Point1))
                    {
                        neighborPoints.Add(HullLines[i].Point1);
                    }
                    else
                    {
                        neighborPoints.Remove(HullLines[i].Point1);
                        removedPoints.Add(HullLines[i].Point1);
                    }
                    if (!neighborPoints.Contains(HullLines[i].Point2))
                    {
                        neighborPoints.Add(HullLines[i].Point2);
                    }
                    else
                    {
                        neighborPoints.Remove(HullLines[i].Point2);
                        removedPoints.Add(HullLines[i].Point2);
                    }
                }
            }

            if (visibleLines.Count == 0)
            {
                HullPointsInside.Add(currentPoint);
                return;
            }

            foreach (var item in visibleLines)
            {
                HullLines.Remove(item);
            }

            foreach (var item in removedPoints)
            {
                HullPointsInside.Add(item);
                HullPoints.Remove(item);
            }


            int index1 = HullPoints.IndexOf(neighborPoints[0]);
            int index2 = HullPoints.IndexOf(neighborPoints[1]);


            if (Utils.VolumeSign2D(HullPoints[index1], HullPoints[index2], currentPoint) < 0)
            {

                HullLines.Add(new Line(HullPoints[index1], currentPoint));
                HullLines.Add(new Line(currentPoint, HullPoints[index2]));
                HullPoints.Insert(index1 + 1, currentPoint);
            }
            else
            {
                HullLines.Add(new Line(HullPoints[index2], currentPoint));
                HullLines.Add(new Line(currentPoint, HullPoints[index1]));

                HullPoints.Insert(index2 + 1, currentPoint);
            }

        }

        private bool ConvexHull3DInitialTriangle(ref List<Vector3> points)
        {
            List<Vector3> hullStart = new List<Vector3>();
            if (points.Count <= 2)
            {
                Debug.Log("Objekt rabi vsaj 4 točke!");
                return false;
            }
            int i = 2;
            while (Utils.IsCollinear(points[i], points[i - 1], points[i - 2]))
            {

                if (i++ == points.Count - 1)
                {
                    Debug.Log("Vse točke so kolinearne!");
                    return false;
                }
            }

            HullPoints.Add(points[i]);

            if (Utils.VolumeSign2D(points[i], points[i - 1], points[i - 2]) < 0)
            {
                HullPoints.Add(points[i - 2]);
                HullPoints.Add(points[i - 1]);
                HullLines.Add(new Line(points[i], points[i - 2]));
                HullLines.Add(new Line(points[i - 2], points[i - 1]));
                HullLines.Add(new Line(points[i - 1], points[i]));
            }
            else
            {
                HullPoints.Add(points[i - 1]);
                HullPoints.Add(points[i - 2]);
                HullLines.Add(new Line(points[i], points[i - 1]));
                HullLines.Add(new Line(points[i - 1], points[i - 2]));
                HullLines.Add(new Line(points[i - 2], points[i]));
            }

            points.RemoveAt(i);
            points.RemoveAt(i - 1);
            points.RemoveAt(i - 2);

            return true;
        }

        private Vector3 GetCenterPoint()
        {
            Vector3 returnPoint = new Vector3();
            foreach (var item in HullPoints)
            {
                returnPoint += item;
            }
            return returnPoint / HullPoints.Count;
        }

        public Mesh GetMesh()
        {
            Mesh returnMesh = new Mesh();
            List<int> triangles = new List<int>();
            List<Vector3> vertecies = new List<Vector3>();

            Vector3 center = GetCenterPoint();
            for (int i = 0; i < HullPoints.Count; i++)
            {
                triangles.Add(i * 3);
                triangles.Add(i * 3 + 1);
                triangles.Add(i * 3 + 2);
                vertecies.Add(center);
                if (i != HullPoints.Count - 1)
                {
                    vertecies.Add(HullPoints[i + 1]);
                }
                else
                {
                    vertecies.Add(HullPoints[0]);
                }
                vertecies.Add(HullPoints[i]);
            }

            returnMesh.vertices = vertecies.ToArray();
            returnMesh.triangles = triangles.ToArray();

            return returnMesh;
        }
    }
}
