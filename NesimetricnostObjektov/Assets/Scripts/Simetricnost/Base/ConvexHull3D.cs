﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Fidersek.Assets.Scripts.Simetricnost.Base
{
    [System.Serializable]
    public class ConvexHull3D
    {
        public List<Tris> HullFaces;
        public List<Vector3> HullPoints;
        public List<Line> HullEdges;
        public List<Vector3> HullPointsInside;

        public List<Plane> SymmtryPlanes;
        public List<Plane> NonSymmtryPlanes;

        public bool generated = false;

        public ConvexHull3D(List<Vector3> points, MeshFilter filter)
        {
            HullPoints = new List<Vector3>();
            HullFaces = new List<Tris>();
            HullEdges = new List<Line>();
            HullPointsInside = new List<Vector3>();

            //Zacetni tetrahedron
            if (!ConvexHull3DInitialTetrahedron(ref points))
            {
                return;
            }

            //Increment tock
            while (points.Count != 0)
            {
                AddPointToHull(points);
            }
            generated = true;

        }

        public void CheckForNonSymmtry()
        {
            Vector3 center = GetCenterPoint();

            SymmtryPlanes = new List<Plane>();
            NonSymmtryPlanes = new List<Plane>();
            List<Plane> possiblePlanes = new List<Plane>();

            foreach (var face in HullFaces)
            {
                AddPlaneNonDuplicate(possiblePlanes, new Plane(center, face.Point1, new Line(face.Point2, face.Point3).GetMiddle()));
                AddPlaneNonDuplicate(possiblePlanes, new Plane(center, face.Point2, new Line(face.Point1, face.Point3).GetMiddle()));
                AddPlaneNonDuplicate(possiblePlanes, new Plane(center, face.Point3, new Line(face.Point2, face.Point1).GetMiddle()));

                AddPlaneNonDuplicate(possiblePlanes, new Plane(center, new Line(face.Point1, face.Point2).GetMiddle(), new Line(face.Point2, face.Point3).GetMiddle()));
                AddPlaneNonDuplicate(possiblePlanes, new Plane(center, new Line(face.Point2, face.Point3).GetMiddle(), new Line(face.Point3, face.Point1).GetMiddle()));
                AddPlaneNonDuplicate(possiblePlanes, new Plane(center, new Line(face.Point3, face.Point1).GetMiddle(), new Line(face.Point1, face.Point2).GetMiddle()));

                AddPlaneNonDuplicate(possiblePlanes, new Plane(center, face.Point1, face.Point2));
                AddPlaneNonDuplicate(possiblePlanes, new Plane(center, face.Point2, face.Point3));
                AddPlaneNonDuplicate(possiblePlanes, new Plane(center, face.Point3, face.Point1));
            }

            foreach (var item in possiblePlanes)
            {
                if (Utils.IsObjectSymmetrical(item, HullPoints))
                {
                    SymmtryPlanes.Add(item);
                }
                else
                {
                    NonSymmtryPlanes.Add(item);
                }
            }
        }

        public void AddPlaneNonDuplicate(List<Plane> uniqePlane,Plane newPlane)
        {
            foreach (var planeInList in uniqePlane)
            {
                if (newPlane.flipped.normal == planeInList.normal || newPlane.normal == planeInList.normal)
                {
                    return;
                }
            }
            uniqePlane.Add(newPlane);
        }

        public void AddPointToHull(List<Vector3> points)
        {
            //Kreiramo list za vidne trikotnike in robe
            List<Tris> visibleFaces = new List<Tris>();
            List<Line> visibleEdges = new List<Line>();

            Vector3 currentPoint = points[0];
            points.RemoveAt(0);

            //Vidni so vsi ki imajo manjši volumen kot 0 
            foreach (var item in HullFaces)
            {
                Double volumeSign = Utils.VolumeSign3D(item, currentPoint);
                if (volumeSign <= 0)
                {
                    if (volumeSign==0 && Utils.IsInTriangle(currentPoint, item))
                    {
                        HullPointsInside.Add(currentPoint);
                        return;
                    }
                    visibleFaces.Add(item);
                    visibleEdges.AddRange(item.Edges);
                }
            }

            //Če ni vidnega trikotnika je točka znotraj objekta
            if (visibleFaces.Count == 0)
            {
                HullPointsInside.Add(currentPoint);
                return;
            }

            //Odstranimo trikotnike znotraj objekta
            foreach (var item in visibleFaces)
            {
                HullFaces.Remove(item);
            }

            List<Line> edgeBoundary = GetVisibleEdgeBoudnary(visibleEdges, visibleFaces);

            HullEdges = HullEdges.Except(visibleEdges.Except(edgeBoundary)).ToList();

            //Dodamo nove trikotnike
            Vector3 centerPoint = GetCenterPoint();
            foreach (var item in edgeBoundary)
            {
                AddFace(item.Point1, item.Point2, currentPoint, centerPoint);
            }
            HullPoints.Add(currentPoint);

            List<Vector3> innerPoints = GetInnerPoints(visibleEdges, edgeBoundary);

            foreach (var item in innerPoints)
            {
                HullPointsInside.Add(item);
                HullPoints.Remove(item);
            }

        }

        private List<Line> GetVisibleEdgeBoudnary(List<Line> visibleEdges, List<Tris> visibleFaces)
        {
            List<Line> returnList = new List<Line>();
            foreach (var item in visibleEdges)
            {
                if (visibleFaces.FindAll(x => x.Edges.Contains(item)).Count == 1)
                {
                    returnList.Add(item);
                }
            }
            return returnList;
        }

        private List<Vector3> GetInnerPoints(List<Line> visibleEdges, List<Line> edgeBoundary)
        {
            HashSet<Vector3> returnList = new HashSet<Vector3>();
            foreach (var item in visibleEdges)
            {
                returnList.Add(item.Point1);
                returnList.Add(item.Point2);
            }
            foreach (var item in edgeBoundary)
            {
                returnList.Remove(item.Point1);
                returnList.Remove(item.Point2);
            }
            return returnList.ToList();
        }

        public Vector3 GetCenterPoint()
        {
            Vector3 returnPoint = new Vector3();
            foreach (var item in HullPoints)
            {
                returnPoint += item;
            }
            return returnPoint / HullPoints.Count;
        }

        private bool ConvexHull3DInitialTetrahedron(ref List<Vector3> points)
        {
            List<Vector3> hullStart = new List<Vector3>();
            if (points.Count <= 3)
            {
                Debug.Log("Objekt rabi vsaj 4 točke!");
                return false;
            }
            int i = 2;
            while (Utils.IsCollinear(points[i], points[i - 1], points[i - 2]))
            {

                if (i++ == points.Count - 1)
                {
                    Debug.Log("Vse točke so kolinearne!");
                    return false;
                }
            }
            //Ustvari prvi face
            Tris face = new Tris(points[i], points[i - 1], points[i - 2]);

            int j = i + 1;

            while (Utils.IsOnPlane(face, points[j]))
            {
                if (j++ == points.Count - 1)
                {
                    Debug.Log("Vse točke so koplanarne!");
                    return false;
                }
            }
            //Dodamo k hull points
            HullPoints.Add(points[i]);
            HullPoints.Add(points[i - 1]);
            HullPoints.Add(points[i - 2]);
            HullPoints.Add(points[j]);

            //Dodamo face
            AddFace(HullPoints[0], HullPoints[1], HullPoints[2], HullPoints[3]);
            AddFace(HullPoints[0], HullPoints[1], HullPoints[3], HullPoints[2]);
            AddFace(HullPoints[0], HullPoints[2], HullPoints[3], HullPoints[1]);
            AddFace(HullPoints[1], HullPoints[2], HullPoints[3], HullPoints[0]);

            //Odstranimo iz lista
            points.RemoveAt(j);
            points.RemoveAt(i);
            points.RemoveAt(i - 1);
            points.RemoveAt(i - 2);

            return true;
        }

        private void AddFace(Vector3 point1, Vector3 point2, Vector3 point3, Vector3 innerPoint)
        {
            Tris face = new Tris(point1, point2, point3);
            if (Utils.VolumeSign3D(face, innerPoint) < 0)
            {
                face.RevertPoints();
            }
            face.Edges.Add(AddEdge(face.Point1, face.Point2));
            face.Edges.Add(AddEdge(face.Point2, face.Point3));
            face.Edges.Add(AddEdge(face.Point3, face.Point1));
            HullFaces.Add(face);
        }

        private Line AddEdge(Vector3 point1, Vector3 point2)
        {
            Line returnLine = HullEdges.Find(x =>
                (x.Point1 == point1 && x.Point2 == point2) ||
                (x.Point1 == point2 && x.Point2 == point1)
            );
            if (returnLine != null)
            {
                return returnLine;
            }
            returnLine = new Line(point1, point2);
            HullEdges.Add(returnLine);
            return returnLine;
        }

        public Mesh GetMesh()
        {
            Mesh returnMesh = new Mesh();
            List<int> triangles = new List<int>();
            List<Vector3> vertecies = new List<Vector3>();

            for (int i = 0; i < HullFaces.Count; i++)
            {
                triangles.Add(i * 3);
                triangles.Add(i * 3 + 1);
                triangles.Add(i * 3 + 2);
                vertecies.Add(HullFaces[i].Point1);
                vertecies.Add(HullFaces[i].Point2);
                vertecies.Add(HullFaces[i].Point3);
            }
            returnMesh.vertices = vertecies.ToArray();
            returnMesh.triangles = triangles.ToArray();

            return returnMesh;
        }
    }
}
