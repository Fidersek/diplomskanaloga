using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fidersek
{
    [System.Serializable]
    public class Tris
    {
        public Vector3 Point1;
        public Vector3 Point2;
        public Vector3 Point3;

        public List<Line> Edges;

        public Tris(Vector3 point1, Vector3 point2, Vector3 point3)
        {
            Point1 = point1;
            Point2 = point2;
            Point3 = point3;
            Edges = new List<Line>();
        }

        public void RevertPoints()
        {
            Vector3 temp = Point2;
            Point2 = Point3;
            Point3 = temp;
        }

    }
}
